#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "md5.h"

const int PASS_LEN=20;        // Maximum any password will be
const int HASH_LEN=33;        // Length of MD5 hash strings

// Given a hash and a plaintext guess, return 1 if
// the hash of the guess matches the given hash.
// That is, return 1 if the guess is correct.
int tryguess(char *hash, char *guess)
{
    int testresult = 0;

    // *** TEST ***
    // printf("Current hash is %s Guess is %s\n", hash, guess);
    // **************   
    
    // Hash the guess using MD5    
    char *temphash = md5(guess, strlen(guess));

    // *** TEST ***
    // printf("Temp hash is %s\n", temphash);
    // **************   

    // Compare the two hashes
    testresult = strcmp(hash, temphash);

    // Free any malloc'd memory
    //free(hash);
    //free(guess);
    
    if (testresult == 0) 
        return 1;
    else
        return 0;
}

// Read in the dictionary file and return the array of strings
// and store the length of the array in size.
// This function is responsible for opening the dictionary file,
// reading from it, building the data structure, and closing the
// file.
char **read_dictionary(char *filename, int *size)
{
    FILE *f = fopen(filename,"r'");
    char line [LINE_LEN];
    int count = 0;
    int lines = DICTIONARY_LINES;
    char **words = malloc(lines * sizeof(char *));     

    // File not found, return NULL with size 0
    if (f == NULL) {
        *size = 0;
        return NULL;
    }

    // File found, process it
    while (fgets(line, LINE_LEN, f) !=NULL)
    {
        if (count == lines)
        {
            lines += DICTIONARY_LINES;
            words = realloc(words, lines * sizeof(char *));
        }
        line[strlen(line)-1] = '\0';
        char *word = malloc(strlen(line) * sizeof(char) + 1);

        strcpy(word, line);
        words[count] = word;
        count++;
    }
    
    // Close the file
    fclose(f);

    // *** TEST ***
    // for (int i=0; i < lines; i++)
    // {
    //    printf("Word Dictionary is %s\n", words[i]);
    // }
    // **************    

    *size = count;
    return words;
}


int main(int argc, char *argv[])
{
    if (argc < 3) 
    {
        printf("Usage: %s hash_file dict_file\n", argv[0]);
        exit(1);
    }

    // Read the dictionary file into an array of strings.
    int i=0, j=1, dlen, result;
    char hashline[LINE_LEN];
    char *hashString;
    char **dictionary_file = read_dictionary(argv[2], &dlen);

    // Open the hash file for reading.
    FILE *h = fopen(argv[1],"r'");
    
    // **** TEST ****
    //printf("argv 1 %s and 2 is %s\n", argv[1], argv[2]);
    //printf("dlen is %d\n", dlen);   
    //printf("Dictionary 77th is %s\n", dictionary_file[76]);
    // **************
    
    // For each hash, try every entry in the dictionary.
    while (fgets(hashline, sizeof hashline, h) !=NULL)
    {
        hashline[strlen(hashline)-1] = '\0';
        hashString = malloc(strlen(hashline) * sizeof(char) +1);
        strcpy(hashString, hashline);
        
        // **** TEST ****
        //printf("Inside while fgets .. Dictionary 77th is %s\n", dictionary_file[76]);        
        printf("TEST %d hashString is %s\n", j++, hashString);
        // **************        
     
        for (i=0; i < dlen; i++)
        {
            result = tryguess(hashString, dictionary_file[i]);

            // Match found
            if (result == 1)
            {
                // Print the matching dictionary entry.
                printf("Match found %d --- %s\n", i, dictionary_file[i]);
            }
        }
    }
    
    // Close the file
    fclose(h);    
}
